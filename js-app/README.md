### Demo Project: ###
Use Docker for local development
### Technologies used: ###
Docker, Node.js, MongoDB, MongoExpress
### Project Description: ###
- Create Dockerfile for Nodejs application and build Docker image
- Run Nodejs application in Docker container and connect to MongoDB database container locally. 
- Also run MongoExpress container as a UI of the MongoDB database.

##### Module 7: Containers with Docker #####

### AWS ECR Repo - Private Docker Repository
Step 1. Create ecr repository on AWS
Step 2. Push image to the ecr repo
Pre-Requisites:
- AWS CLI is installed
- Credentials configured
build docker image from Dockerfile
docker build -t my-app:1.0 .
docker tag imageName:tag repo_address/imageName:tag
docker push repo_address/imageName:tag
docker-compose -f mongo.yaml up

### Docker Volumes
Mounts host file system to container virtual file system
3 Volumes types
 docker run -v /home/mount/data(host file system):/var/lib/mysql/data(container file system) - Host volume
 OR
 docker run -v /var/lib/mysql/data(container file system) - Anonymous Volume
 OR
 docker run -v named:/var/lib/mysql/data(container file system) - named Volume (should use)

 create in docker-compose


To access the shell of the Docker VM in order to view volume information, use this command:
 docker run -it --privileged --pid=host debian nsenter -t 1 -m -u -n -i sh


### Create Docker Hosted repo on Nexus
Create user role for Docker repo
Assign role to the Nexus user
Configure HTTP and Port for Docker repo
Configure Port on Droplet firewall
Activate Docker Bearer token in Realms on Nexus

_Configure Docker Desktop to allow insecure regiestries_
 Settings -> Docker Engine - "insecure-registries": ["nexus_REPO_address: PORT"]

docker login nexus_REPO_address:PORT
build docker image
tag image docker tag imageName:tag nexus_REPO_address:PORT/imageName:tag
docker push nexus_REPO_address:PORT/imageName:tag

_Fetch Docker images from Nexus_
 curl -u username:pwd -X 'http://nexus_REPO_address:PORTservice/rest/v1/components?repository=repoName'

### Deploy Nexus as Docker Container
Create a Droplet - ssh to it
Install Docker on the droplet
Configure Volume - follow Nexus image doc on Docker Hub
 docker volume create --name nexus-data
docker run -d -p --name nexus -v nexus-data:/nexus-data sonatype/nexus3
docker volume ls
docker inspect nexus-data - use it to find admin.password



docker pull mongo
docker pull mongo-express
docker network create mongo-network
docker run -p 2017:2017 
 1  apt update
    2  java
    3  apt install openjdk-8-jre-headless
    4  adduser chol
    5  chown --help
    6  cd /opt
    7  ls
    8  wget https://download.sonatype.com/nexus/3/nexus-3.56.0-01-unix.tar.gz
    9  ls
   10  tar -zvxf nexus-3.56.0-01-unix.tar.gz
   11  ls
   12  chown -R chol:chol sonatype-work
   13  chown -R chol:chol nexus-3.56.0-01
   14  ls -l
   15  history
   16  vim nexus-3.53.0-01/bin/nexus.rc
   17  vim nexus-3.56.0-01/bin/nexus.rc
   18  su - chol
   19  apt install net-tools
   20  su - chol
   21  history
   22* 
   23  exit
   24  ls
   25  su - chol
   26  usermod -aG sudo chol
   27  netstat -lpnt
   28  su - chol
   29  history


       1  /opt/nexus-3.56.0-01/bin/nexus start
    2  ps aux | grep nexus
    3  netstat
    4  apt install net-tools
    5  exit
    6  ps aux | grep nexus
    7  netstat -lnpt
    8  history
    9  exit
   10  ls /opt/sonatype-work/nexus3
   11  cat /opt/sonatype-work/nexus3/admin.password
   12  /opt/nexus-3.56.0-01/bin/nexus stop
   13  exit
   14  ls
   15  cd /opt
   16  ls
   17  nexus-3.56.0-01/bin/nexus start
   18  netstat -lpnt
   19  ls -l
   20  ps
   21  cd ..
   22  exit
   23  nexus-3.56.0-01/bin/nexus stop
   24  cd /opt
   25  nexus-3.56.0-01/bin/nexus stop
   26  nexus-3.56.0-01/bin/nexus start
   27  netstat -lpnt
   28  ps
   29  netstat -lpnt
   30  cd ..
   31  exit
   32  history

   deploy nexus as a docker container on digital ocean by first installing docker on the remote server.
#=========================================================

## demo app - developing with Docker

This demo app shows a simple user profile app set up using 
- index.html with pure js and css styles
- nodejs backend with express module
- mongodb for data storage

All components are docker-based

### With Docker

#### To start the application

Step 1: Create docker network

    docker network create mongo-network 

Step 2: start mongodb 

    docker run -d -p 27017:27017 -e MONGO_INITDB_ROOT_USERNAME=admin -e MONGO_INITDB_ROOT_PASSWORD=password --name mongodb --net mongo-network mongo    

Step 3: start mongo-express
    
    docker run -d -p 8081:8081 -e ME_CONFIG_MONGODB_ADMINUSERNAME=admin -e ME_CONFIG_MONGODB_ADMINPASSWORD=password --net mongo-network --name mongo-express -e ME_CONFIG_MONGODB_SERVER=mongodb mongo-express   

_NOTE: creating docker-network in optional. You can start both containers in a default network. In this case, just emit `--net` flag in `docker run` command_

Step 4: open mongo-express from browser

    http://localhost:8081

Step 5: create `user-account` _db_ and `users` _collection_ in mongo-express

Step 6: Start your nodejs application locally - go to `app` directory of project 

    cd app
    npm install 
    node server.js
    
Step 7: Access you nodejs application UI from browser

    http://localhost:3000

### With Docker Compose

#### To start the application

Step 1: start mongodb and mongo-express

    docker-compose -f docker-compose.yaml up
    
_You can access the mongo-express under localhost:8080 from your browser_
    
Step 2: in mongo-express UI - create a new database "my-db"

Step 3: in mongo-express UI - create a new collection "users" in the database "my-db"       
    
Step 4: start node server 

    cd app
    npm install
    node server.js
    
Step 5: access the nodejs application from browser 

    http://localhost:3000

#### To build a docker image from the application

    docker build -t my-app:1.0 .       
    
The dot "." at the end of the command denotes location of the Dockerfile.
